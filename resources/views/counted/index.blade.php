@extends('layouts.app')

@section('content')

  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="far fa-user nav-icon"></i> Countable Numbers</h1>
          </div>
          
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Countable</h3>
              </div>
            
              <div class="card-body">
                  <div class="input-group mb-3">
                  <input type="number" id="terbilang-input" class="form-control" onkeyup="inputTerbilang();" placeholder="Input Number">
                  </div>
                  <div class="input-group mb-3">
                    
                  <input id="terbilang-output" name="terbilang-output" type="text" class="form-control" readonly placeholder="Return Countable">
                  </div>
                  
                  
              </div>
                
            </div>
          </div>
         
          
        </div>
       
        
      </div>
    </section>
@endsection
