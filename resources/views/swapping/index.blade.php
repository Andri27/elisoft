@extends('layouts.app')

@section('content')
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="far fa-user nav-icon"></i> Swapping Two variable</h1>
          </div>
          
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          
          <div class="col-md-6">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Swapping Value</h3>
              </div>
              <form class="form-horizontal" method="post" action="{{ route('swapping.store') }}" >
                @csrf
                <div class="card-body">
                    <div class="input-group mb-3">
                      <input id="nilai_a" name="nilai_a" type="number" class="form-control" placeholder="Input Value A">
                    </div>
                    <div class="input-group mb-3">
                      
                    <input id="nilai_b" name="nilai_b" type="number" class="form-control" placeholder="Input Value B">
                    </div>
                    
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <!-- <button type="submit" class="btn btn-info">Create</button> -->
                  <input type="submit" name="submit" value="Swapping" class="btn btn-info" /> 
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
          </div>
          
        </div>
       
        
      </div>
    </section>
@endsection
