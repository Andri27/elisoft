@extends('layouts.app')

@section('content')
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="far fa-user nav-icon"></i> Swapping Two variable</h1>
          </div>
          
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
        
          <div class="col-md-12">
            <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title">
                  Swipping Proccess
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                $A = {{ $nilai_a }}; <br>
                $B = {{ $nilai_b }}; <br>
                Proses 1:<br>
                    &nbsp;&nbsp;&nbsp;$A = $A({{ $nilai_a }}) + $B({{ $nilai_b }})  => Hasil A Adalah {{ $hasil1 }}<br>
                Proses 2:<br>
                    &nbsp;&nbsp;&nbsp;$B = $A({{ $hasil1 }}) - $B({{ $nilai_b }})  => Hasil B Adalah {{$hasil2}}<br>
                Proses 3:<br>
                    &nbsp;&nbsp;&nbsp;$A = $A({{ $hasil1 }}) - $B({{$hasil2}})  => Hasil A Adalah {{ $hasil3 }}<br><br>

                Nilai Awal A Adalah {{ $nilai_a }}  || Hasil Akhir A Adalah {{$hasil3}}<br>
                Nilai Awal B Adalah {{ $nilai_b }}  || Hasil Akhir B Adalah {{$hasil2}}<br>
              </div>
              
            </div>
          
          </div>
          
        </div>
       
        
      </div>
    </section>
@endsection
