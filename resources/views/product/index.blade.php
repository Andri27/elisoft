@extends('layouts.app')

@section('content')
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="far fa-user nav-icon"></i> Product Stock</h1>
          </div>
          
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Ketentuan Return Response API & Mengimplementasikan ke Datatables </h5>
                1. Hit Api http://149.129.221.143/kanaldata/Webservice/bank_account dengan form-data bank_id nya adalah 2
            </div>
          </div>
          <div class="col-12">
            
              <div class="card card-outline card-info">
                <div class="card-header">
                  <h3 class="card-title">
                    Return Response API 
                  </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  {{$response}}
                  
                </div>
                
              </div>
            
          </div>
          <div class="col-12">
          
            <div class="card">
              <div class="card-body">
              Mengimplementasikan ke Datatables
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>  
                          <th>No</th>
                          <th>Id</th>
                          <th>Bank Id</th>
                          <th>Account Name</th>
                          <th>Account Number</th>
                          <th>Status</th>
                          <th>Info</th>
                      </tr>
                    </thead>
                    <?php $no = 1 ?>
                    @foreach ($users as $field)
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$field['id']}}</td>
                        <td>{{$field['bank_id']}}</td>
                        <td>{{$field['account_name']}}</td>
                        <td>{{$field['account_number']}}</td>
                        <td>{{$field['status']}}</td>
                        <td>{{$field['info']}}</td>
                        
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                  
                </table>
              </div>
            </div>
          </div>
          <div class="col-1"></div>
        </div>
        
      </div>
    </section>
  
    
 
@endsection
