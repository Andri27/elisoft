@extends('layouts.app')

@section('content')
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add New</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><i class="nav-icon fas fa-users"></i> Manage Users</li>
              <li class="breadcrumb-item active"><i class="far fa-user nav-icon"></i> Users</li>
              <li class="breadcrumb-item active">Add New</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-2"></div>
          <div class="col-md-8">
            
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">New user</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" action="{{ route('user.store') }}" >
                @csrf
                <div class="card-body">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">@</span>
                      </div>
                      <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Username">
                    </div>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                      </div>
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                    </div>
                    <div class="input-group mb-3">
                      <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                          </div>
                      </div>
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    </div>
                    <div class="input-group mb-3">
                      <div class="input-group-append">
                          <div class="input-group-text">
                            <i class="fa fa-cog fa-spin"></i>
                          
                          </div>
                      </div>
                      <select class="form-control" name="level" id="level" />
                        @foreach($levels as $key => $val)
                          @if($key==old('level'))
                            <option value="{{ $key }}" selected>{{ $val }}</option>
                          @else
                            <option value="{{ $key }}">{{ $val }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Create</button>
                  <a href="{{ route('user.index') }}" class="btn btn-default float-right">Cancel</a>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>

          </div>
          <div class="col-md-2"></div>
         
        </div>
      </div>
    </section>

 
@endsection
