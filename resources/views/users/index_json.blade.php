@extends('layouts.app')

@section('content')
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="far fa-user nav-icon"></i> Users Format Json</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><i class="nav-icon fas fa-users"></i> API User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Ketentuan Response All User In Format Json </h5>
                Reponse Json Ini akan bertambah/berubah/berkurang sesuai dengan data user, untuk perubahan silahkan operasionalkan menu Users.
            </div>
          </div>
          <div class="col-md-12">
            <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title">
                  Response All User In Format Json
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                {{ $data }}
              </div>
              
            </div>
          </div>
          <div class="col-md-12">
          
            
            <div class="card">
              <!-- <div class="card-header">
                <h3 class="card-title"><a href="{{ url('/add/users') }}" class="btn btn-block btn-outline-success">Add New</a></h3>
              </div> -->
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th>No</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Level</th>
                      </tr>
                    </thead>
                    <?php $no = 1 ?>
                    @foreach($rows as $row)
                      <tr>
                      <td>{{ $no++ }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->isadmin }}</td>
                          
                      </tr>
                    @endforeach
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          
          </div>
        </div>
      </div>
    </section>
    
    
 
@endsection
