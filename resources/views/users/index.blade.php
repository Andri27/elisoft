@extends('layouts.app')

@section('content')
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"></body>
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="far fa-user nav-icon"></i> Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><i class="nav-icon fas fa-users"></i> Manage Users</li>
              <li class="breadcrumb-item active"><i class="far fa-user nav-icon"></i> Users</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            
            <div class="card">
              <!-- <div class="card-header">
                <h3 class="card-title"><a href="{{ url('/add/users') }}" class="btn btn-block btn-outline-success">Add New</a></h3>
              </div> -->
              <div class="card-header">
                <form class="form-inline">
                    <!-- <div class="form-group mr-1">
                        <input class="form-control" type="text" name="q" value="{{ $q}}" placeholder="Pencarian..." />
                    </div>
                    <div class="form-group mr-1">
                        <button class="btn btn-success">Refresh</button>
                    </div> -->
                    <div class="form-group mr-1">
                        <a class="btn btn-primary" href="{{ route('user.create') }}">Tambah </a>
                    </div>
                </form>
            </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th>No</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Level</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <?php $no = 1 ?>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->isadmin }}</td>
                        <td>
                            <a class="btn btn-sm btn-warning" href="{{ route('user.edit', $row) }}"><i class="fa fa-edit"></i></a>
                            <form method="POST" action="{{ route('user.destroy', $row) }}" style="display: inline-block;">
                                @csrf
                                @method('DELETE')
                                
                                <button class="btn btn-sm btn-danger" onclick="return confirm('Hapus Data?')"><i class='fa fa-trash'></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                  
                </table>

              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <div class="col-1"></div>
        </div>
        <div class="row">
        <div class="col-1"></div>
        <div class="col-md-10">
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-exclamation-triangle"></i>
                  Alerts
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Ketentuan</h5>
                  Tidak Bisa MengHapus User Yang Sedang Login
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <div class="col-1"></div>
        
      </div>
    </section>
  
    
 
@endsection
