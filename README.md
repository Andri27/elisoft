# Tutorial Penggunaan
1. Setelah Berhasil Mengcloning, Buat env baru copy paste dari env.example ubah menjadi env
2. buat database baru di mysql dengan nama sesuai keinginan, kemudian koneksikan di env
3. jalankan perintah _**composer update**_
4. jalankan perintah _**php artisan key:generate**_
5. terakhir jalankan perintah migrate _**php artisan migrate**_ 

Jika mau langsung import database tanpa menjalankan perintah point 5, sudah disediakan database dengan nama laravel8, yang berada di directory database->laravel8.sql

> username/email : andriansyah.job27@gmail.com
> password       : admin123


# Register
- di kanan atas ada menu login & register, klik menu register untuk daftar sebagai user, setelah berhasil silahkan login dengan user yang telah berhasil di daftarkan, terimakasih.



