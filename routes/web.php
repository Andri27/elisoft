<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserJsonController;
use App\Http\Controllers\SwappingController;
use App\Http\Controllers\CountedController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard1', [App\Http\Controllers\DashboardController::class, 'dashboard1'])->name('dashboard1');
Route::get('/dashboard2', [App\Http\Controllers\DashboardController::class, 'dashboard2'])->name('dashboard2');
Route::get('/dashboard3', [App\Http\Controllers\DashboardController::class, 'dashboard3'])->name('dashboard3');
Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('index');
Route::get('/add/users', [App\Http\Controllers\UserController::class, 'create'])->name('create');
Route::post('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('store');


Route::resource('user', UserController::class);
Route::resource('product', ProductController::class);
Route::resource('userjson', UserJsonController::class);
Route::resource('swapping', SwappingController::class);
Route::resource('counted', CountedController::class);
