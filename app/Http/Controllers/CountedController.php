<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CountedController extends Controller
{
    public function index(Request $request){
        
       
        return view('counted.index');
    }

    public function store(Request $request){
        $nilai_a = $request->nilai_a;
        $nilai_b = $request->nilai_b;

        $data['nilai_a'] = $nilai_a;
        $data['nilai_b'] = $nilai_b;
        $data['hasil1'] = intval($nilai_a) + intval($nilai_b);
        $data['hasil2'] = intval($data['hasil1']) - intval($nilai_b);
        $data['hasil3'] = intval($data['hasil1']) - intval($data['hasil2']);
        
        return view('swapping.swapping',$data);
    }
}
