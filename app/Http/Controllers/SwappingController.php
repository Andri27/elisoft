<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SwappingController extends Controller
{
    
    public function index(Request $request){
        $response = response()->json([
            'status' => Response::HTTP_OK,
            'message'=> 'Get User Format Json',
            'data'   => User::all()
        ],Response::HTTP_OK);
        $data = $response->original;
       
        return view('swapping.index')->with('data', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    public function store(Request $request){
        $nilai_a = $request->nilai_a;
        $nilai_b = $request->nilai_b;

        $data['nilai_a'] = $nilai_a;
        $data['nilai_b'] = $nilai_b;
        $data['hasil1'] = intval($nilai_a) + intval($nilai_b);
        $data['hasil2'] = intval($data['hasil1']) - intval($nilai_b);
        $data['hasil3'] = intval($data['hasil1']) - intval($data['hasil2']);
        
        return view('swapping.swapping',$data);
    }
}
