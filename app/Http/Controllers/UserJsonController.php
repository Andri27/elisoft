<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UserJsonController extends Controller
{
    
    public function index(Request $request){
        $response = response()->json([
            'status' => Response::HTTP_OK,
            'message'=> 'Success Message',
            'data'   => User::all()
        ],Response::HTTP_OK);
        $data = $response->original;
        $tampil['rows'] = User::all();
       
        return view('users.index_json',$tampil)->with('data', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }
}
